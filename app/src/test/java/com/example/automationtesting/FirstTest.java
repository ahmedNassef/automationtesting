package com.example.automationtesting;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;

public class FirstTest {


    AndroidDriver driver;

    @Before
    public void setUp() throws MalformedURLException {
        // Created object off DesiredCapabilities class.
        DesiredCapabilities capabilities = new DesiredCapabilities();

        // Set android deviceName desired capability. Set your device name.
//        capabilities.setCapability("deviceName", "EMULATOR-5554");
        capabilities.setCapability("deviceName", "SM-T285");
        //capabilities.setCapability("deviceName", "Lenovo A7010a48");


        // Set BROWSER_NAME desired capability. It's Android in our case here.
//        capabilities.setCapability("browserName", "Android");

        capabilities.setCapability("app", "F:\\MsalesWorkSpaceFinal\\app\\ElAmir\\release\\app-ElAmir-release.apk");
       // capabilities.setCapability("app", "D:\\ElAmir\\release\\app-ElAmir-release.apk");
        // Set android VERSION desired capability. Set your mobile device's OS version.
//        capabilities.setCapability("version", "8.1.0");
        capabilities.setCapability("version", "6.0");

        // Set android platformName desired capability. It's Android in our case here.
        capabilities.setCapability("platformName", "Android");

        capabilities.setCapability("autoGrantPermissions", true);
        capabilities.setCapability("appPackage", "com.emeint.android.msales");
        capabilities.setCapability("appWaitPackage", "com.emeint.android.msales");
        capabilities.setCapability("appWaitActivity", "com.emeint.android.msales.view.*");


        // Set android appPackage desired capability. It is
        // com.android.calculator2 for calculator application.
        // Set your application's appPackage if you are using any other app.


        // Set android appActivity desired capability. It is
        // com.android.calculator2.Calculator for calculator application.
        // Set your application's appPackage if you are using any other app.
        capabilities.setCapability("MainActivity", "com.emeint.android.msales.view.StartupActivity");

        // Created object of RemoteWebDriver will all set capabilities.
        // Set appium server address and port number in URL string.
        // It will launch calculator app in android device.
        driver = new AndroidDriver<>(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);


    }

    @Test
    public void testFirstStartUp() {


        if(driver.currentActivity().toLowerCase().contains("settingsactivity")){

            driver.findElementById("com.emeint.android.msales:id/company_name_spinner").click();
            driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ListView/android.widget.CheckedTextView[1]").click();

            (new TouchAction(driver))
                    .press(584, 1832)
                    .moveTo(786, 298)
                    .release()
                    .perform();

            (new TouchAction(driver))
                    .press(962, 1870)
                    .moveTo(424, 80)
                    .release()
                    .perform();

            (new TouchAction(driver))
                    .press(803, 1824)
                    .moveTo(370, 193)
                    .release()
                    .perform();

//        (new TouchAction(driver)).tap(882, 1849).perform();
            driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView").click();
            driver.findElementById("com.emeint.android.msales:id/setting_print_bluetooth").click();



            driver.findElementById("com.emeint.android.msales:id/settings_save_button").click();

        }

        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
    }

    @Test
    public void testLogin(){
        if(driver.currentActivity().toLowerCase().contains("loginactivity")){

            driver.findElementById("com.emeint.android.msales:id/mobile_number_edit_text").click();
            driver.findElementById("com.emeint.android.msales:id/mobile_number_edit_text").sendKeys("20100010102");
            driver.findElementById("com.emeint.android.msales:id/username_edit_text").sendKeys("CTSH03");
            driver.findElementById("com.emeint.android.msales:id/password_edit_text").sendKeys("123");


            driver.navigate().back();

            driver.findElementById("com.emeint.android.msales:id/remember_me_checkbox").click();
            driver.findElementById("com.emeint.android.msales:id/login_button").click();

            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);

        }
    }



    @Test
    public void testVisit(){
        MobileElement el1 = (MobileElement) driver.findElementById("android:id/button1");
        el1.click();
        MobileElement el2 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/skip_button");
        el2.click();

        //////
        MobileElement el3 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ExpandableListView/android.widget.LinearLayout[2]/android.widget.GridView/android.widget.LinearLayout[2]/android.widget.LinearLayout");
        el3.click();
        MobileElement el4 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/ADD_ITEM_BUTTON");
        el4.click();
        MobileElement el5 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.LinearLayout[4]/android.widget.LinearLayout");
        el5.click();
        MobileElement el6 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout[1]");
        el6.click();
        MobileElement el7 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.HorizontalScrollView/android.widget.RadioGroup/android.widget.RadioButton[1]");
        el7.click();
        MobileElement el8 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.HorizontalScrollView/android.widget.RadioGroup/android.widget.RadioButton[2]");
        el8.click();
        MobileElement el9 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.HorizontalScrollView/android.widget.RadioGroup/android.widget.RadioButton[1]");
        el9.click();
        MobileElement el10 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.HorizontalScrollView/android.widget.RadioGroup/android.widget.RadioButton[2]");
        el10.click();
        MobileElement el11 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.HorizontalScrollView/android.widget.RadioGroup/android.widget.RadioButton[1]");
        el11.click();
        (new TouchAction(driver))
                .press( 380,  1183)
  .moveTo( 394, 171)
  .release()
                .perform();

        MobileElement el12 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.HorizontalScrollView/android.widget.RadioGroup/android.widget.RadioButton[1]");
        el12.click();
        MobileElement el13 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.HorizontalScrollView/android.widget.RadioGroup/android.widget.RadioButton[2]");
        el13.click();
        MobileElement el14 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.HorizontalScrollView/android.widget.RadioGroup/android.widget.RadioButton[1]");
        el14.click();
        MobileElement el15 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.HorizontalScrollView/android.widget.RadioGroup/android.widget.RadioButton[2]");
        el15.click();
        MobileElement el16 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.HorizontalScrollView/android.widget.RadioGroup/android.widget.RadioButton[1]");
        el16.click();
        MobileElement el17 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.HorizontalScrollView/android.widget.RadioGroup/android.widget.RadioButton[2]");
        el17.click();
        (new TouchAction(driver))
                .press( 341,  1206)
  .moveTo(380, 697)
  .release()
                .perform();

        (new TouchAction(driver))
                .press( 366,  380)
  .moveTo( 380 ,962)
  .release()
                .perform();

        MobileElement el18 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/BA_DONE_BUTTON");
        el18.click();
        MobileElement el19 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout[2]");
        el19.click();
        MobileElement el20 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/SALES_DOC_CUSTOM_TAB_ITEMS_BUTTON");
        el20.click();
        MobileElement el21 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/ADD_MULTIPLE_ITEM_BUTTON");
        el21.click();
        MobileElement el22 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout[2]");
        el22.click();
        MobileElement el23 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/clear_ordered_imageView");
        el23.click();
        MobileElement el24 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/required_editText");
        el24.sendKeys("4");
        MobileElement el25 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/done_button");
        el25.click();
        MobileElement el26 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout[2]");
        el26.click();
        MobileElement el27 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/clear_ordered_imageView");
        el27.click();
        MobileElement el28 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/required_editText");
        el28.sendKeys("4");
        MobileElement el29 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/done_button");
        el29.click();
        MobileElement el30 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/other_best_sku_imageView");
        el30.click();
        MobileElement el31 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/FILTERED_LIST_TITLE_DONE");
        el31.click();
        MobileElement el32 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/BA_DONE_BUTTON");
        el32.click();
        MobileElement el33 = (MobileElement) driver.findElementById("android:id/button1");
        el33.click();
        MobileElement el34 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/BA_DONE_BUTTON");
        el34.click();
        MobileElement el35 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/VISIT_SUBMIT_BUTTON");
        el35.click();
        MobileElement el36 = (MobileElement) driver.findElementById("android:id/button2");
        el36.click();
        MobileElement el37 = (MobileElement) driver.findElementById("com.emeint.android.msales:id/error_cancel_button");
        el37.click();

    }


    @After
    public void End() {
        driver.quit();
    }


}
